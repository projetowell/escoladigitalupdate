create schema escoladigitalupdate;

use escoladigitalupdate;
 
CREATE TABLE product (
    id int PRIMARY KEY, 
    `name` varchar(255), 
    `level` varchar(255),
    price money,
    discount money 0,
    product_category_id int NOT NULL,
    FOREIGN KEY (product_category_id) REFERENCES product_category(id)
);
 
CREATE TABLE purchase_list (
    id int PRIMARY KEY,
    product_id int NOT NULL, 
    user_id int NOT NULL,
    purchase_order_id int NULL,
    FOREIGN KEY (product_id) REFERENCES product(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);
 
CREATE TABLE purchase_order (
    id int PRIMARY KEY,
    product_id int NOT NULL, 
    order_status varchar(255),
    payment_method varchar(255) NOT NULL,
    user_id int NOT NULL,
    FOREIGN KEY (product_id) REFERENCES product(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE card_user (
    id int PRIMARY KEY,
    flag varchar(255) NOT NULL, 
    owner_name varchar(255) NOT NULL, 
    cpf varchar(15) NOT NULL, 
    card_number varchar(20) NOT NULL, 
    cvv varchar(4) NOT NULL, 
    user_id int NOT NULL, 
    classroom_id int NOT NULL,
    FOREIGN KEY (classroom_id) REFERENCES classroom_id(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE product_category (
    id int PRIMARY KEY,
    `name` varchar(255) NOT NULL
);

CREATE TABLE classroom (
    id int PRIMARY KEY,
    product_id int NOT NULL, 
    class_name varchar(255) NOT NULL,
    class_url varchar(255) NOT NULL,
    class_description text NOT NULL,
    instructor_id int NOT NULL,
    FOREIGN KEY (product_id) REFERENCES product(id),
    FOREIGN KEY (instructor_id) REFERENCES user(id)
);

CREATE TABLE classroom_progress (
    id int PRIMARY KEY,
    classroom_id int NOT NULL, 
    user_id int NOT NULL,
    progress int NOT NULL,
    FOREIGN KEY (classroom_id) REFERENCES classroom_id(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE classroom_rating (
    id int PRIMARY KEY,
    classroom_id int NOT NULL, 
    user_id int NOT NULL,
    rating int NOT NULL,
    comment varchar(255) NOT NULL,
    FOREIGN KEY (classroom_id) REFERENCES classroom_id(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE card_user (
    id int PRIMARY KEY,
    flag varchar(255) NOT NULL, 
    owner_name varchar(255) NOT NULL, 
    cpf varchar(15) NOT NULL, 
    card_number varchar(20) NOT NULL, 
    cvv varchar(4) NOT NULL, 
    user_id int NOT NULL, 
    classroom_id int NOT NULL,
    FOREIGN KEY (classroom_id) REFERENCES classroom_id(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);
