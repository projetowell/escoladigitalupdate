@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-5 col-md-3">
            <iframe width="100%" height="auto" src="https://www.youtube.com/embed/PrkAOfN4aa0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div class="col-5 col-md-3">
            <iframe width="100%" height="auto" src="https://www.youtube.com/embed/6kAga6Onnjg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div class="col-5 col-md-3">
        <iframe width="100%" height="auto" src="https://www.youtube.com/embed/YfuDu4j6yjQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>        </div>

            

    </div>
</div>
@endsection
