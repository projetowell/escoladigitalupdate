@extends('layouts.landing')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-8">
            <h2>Reset Morfológico</h2>
            <p>Você vai resetar tudo que aprendeu sobre Anatomia Dental, ou seja, um processo onde vai desconstruir toda a complicação de informações desnecessárias para ficar no simples e efetivo, em 4 passos.</p>
            <a class="bg-danger text-white py-1 px-3 rounded" href="{{ route('register') }}">{{ __('Quero me inscrever') }}</a>

        </div>
        <div class="col-4">
            <img class="img-fluid" src="/img/landing-page-1.webp"/>
        </div>
    </div>
    
    <div class="row justify-content-center bg-black text-white text-center my-5 py-4">
        <h2>TREINAMENTO COMPLETO  |  AO VIVO E GRATUITO  |  04 A 06 DE JULHO</h2>
    </div>

    <div class="row justify-content-center text-center">
        <p class="py-4">Você está convocado para marcar presença  no movimento de aprendizagem prática e objetiva em morfologia dental, para ingressar no mundo da Reabilitação Oral.</p>
        <p class="py-4">Você foi bombardeado por uma tonelada de teorias e métodos nos primeiro anos da sua faculdade de Odontologia. Com isso, muitos detalhes se perderam em sua memória.</p>
        <p class="py-4">Todo esse excesso de informação te deixa confuso e te faz jogar no lixo o recurso mais valioso da vida, o tempo.</p>
        <p class="py-4">O RESET MORFOLÓGICO não só vai te fazer recordar de todos esses detalhes morfológicos, como te fará ter clareza para executar o simples que dá resultados, farão dentes que se pareçam com dentes.</p>
        <p class="py-4 bg-danger text-white">De 04 a 06 de Julho, você vai aprender com uma didática fácil que vai acontecer em 3 dias.</p>
    </div>

    <div class="d-flex align-items-center justify-content-between text-center">
        <div class="col-3 d-flex flex-column">
            <h1>1</h1>
            <h2>Resetando</h2>
            <p>Vamos descomplicar o que muitos complicam.</p>
        </div>
        <div class="col-3 d-flex flex-column">
            <h1>2</h1>
            <h2>Inicializando</h2>
            <p>Depois de limpar sua mente do desnecessário é hora de iniciar um novo , pratico e simplificado processo de aprendizagem em anatomia & textura dental.</p>
        </div>
        <div class="col-3 d-flex flex-column">
            <h1>3</h1>
            <h2>Atualizando</h2>
            <p>Com sua mente resetada, organizada e inicializada chega o momento da atualização, é onde você vai aperfeiçoar suas técnicas e visão de como simplificar a morfologia, atingindo a liberdade. Fazer dentes que se pareçam com dentes.</p>
        </div>
    </div>

    <div class="row justify-content-center text-center my-5">
        <a class="bg-danger text-white py-1 px-3 rounded" href="{{ route('register') }}">{{ __('Quero me inscrever') }}</a>
    </div>

    <div class="row justify-content-center text-center">
        <h1>o RESET MORFOLÓGICO é obrigatório para quem</h1>
    </div>

    <div class="d-flex align-items-center justify-content-between text-center">
        <div class="col-3 d-flex flex-column">
            <svg preserveAspectRatio="xMidYMid meet" data-bbox="31.3 19.98 137.434 160.02" xmlns="http://www.w3.org/2000/svg" width="200" height="200" viewBox="31.3 19.98 137.434 160.02" data-type="shape" role="presentation" aria-hidden="true" aria-labelledby="svgcid--yty84p6xlxvv"><title id="svgcid--yty84p6xlxvv"></title>
                <g>
                    <path d="M118.4 56.1l-66.3 79 19.8 16.6 66.3-79-19.8-16.6zm5.5-6.5l19.8 16.6L155 52.6c.3-.4.9-1.1 1.6-2.2 1.1-1.8 2.1-3.8 2.7-5.8 1.7-5.2.8-9.5-3.4-13.1-4.3-3.6-8.7-3.7-13.5-1.1-1.8 1-3.6 2.3-5.2 3.7l-1.9 1.9-11.4 13.6zM65.2 157.5l-17.6-14.8-4.8 23.6 22.4-8.8zm96.2-132.6c7.2 6.1 8.8 14 6.1 22.3-1.5 4.7-3.9 8.5-5.9 10.9L73.1 163.6 31.3 180l8.9-44 88.5-105.5c2-2.4 5.3-5.4 9.7-7.7 7.7-4.1 15.8-4 23 2.1z"></path>
                </g>
            </svg>
            <p>Está saindo da faculdade agora e quer adquirir conhecimento profissional para executar futuras Reabilitações Orais, desde as mais simples às mais complexas.</p>
        </div>
        <div class="col-3 d-flex flex-column">
            <svg preserveAspectRatio="xMidYMid meet" data-bbox="56 23 88 154" viewBox="56 23 88 154" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="shape" role="presentation" aria-hidden="true" aria-labelledby="svgcid-v9fq3s-tlgl51"><title id="svgcid-v9fq3s-tlgl51"></title>
                <g>
                    <path d="M116.667 96H104V57.545h29.333a4 4 0 0 0 0-8H104V27a4 4 0 0 0-8 0v22.545H83.333C68.262 49.545 56 61.76 56 76.773S68.262 104 83.333 104H96v38.455H60a4 4 0 0 0 0 8h36V173a4 4 0 0 0 8 0v-22.545h12.667c15.071 0 27.333-12.214 27.333-27.228S131.738 96 116.667 96zm-33.334 0C72.673 96 64 87.375 64 76.773s8.673-19.228 19.333-19.228H96V96H83.333zm33.334 46.455H104V104h12.667c10.66 0 19.333 8.625 19.333 19.227s-8.673 19.228-19.333 19.228z"></path>
                </g>
            </svg>
            <p>Irá planejar seus próprios casos, sem depender de terceiros, trazendo mais segurança e melhor faturamento para si mesmo.</p>
        </div>
        <div class="col-3 d-flex flex-column">
            <svg preserveAspectRatio="xMidYMid meet" data-bbox="20 40.771 160 118.458" viewBox="20 40.771 160 118.458" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="shape" role="presentation" aria-hidden="true" aria-labelledby="svgcid-jywx9trnetdn"><title id="svgcid-jywx9trnetdn"></title>
                <g>
                    <path d="M177.823 42.952c-1.404-1.406-3.272-2.181-5.26-2.181s-3.856.775-5.259 2.179l-98.323 98.322-36.283-36.28c-1.404-1.406-3.272-2.181-5.26-2.181s-3.856.775-5.258 2.178a7.383 7.383 0 0 0-2.18 5.259c0 1.988.774 3.855 2.179 5.259l41.543 41.543a7.488 7.488 0 0 0 5.259 2.179 7.486 7.486 0 0 0 5.259-2.179L177.82 53.468c1.406-1.404 2.18-3.272 2.18-5.259s-.774-3.856-2.177-5.257z"></path>
                </g>
            </svg>
            <p>Quer dar o primeiro passo na Reabilitação Oral de maneira correta, para futuramente realizar tratamentos de pelo menos 20 mil reais.</p>
        </div>
        <div class="col-3 d-flex flex-column">
            <svg preserveAspectRatio="xMidYMid meet" data-bbox="20.009 20.009 159.982 159.982" viewBox="20.009 20.009 159.982 159.982" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="shape" role="presentation" aria-hidden="true" aria-labelledby="svgcid-hywse7p9ihxo"><title id="svgcid-hywse7p9ihxo"></title>
                <g>
                    <path d="M20.009 91.815l159.982-71.806-71.806 159.982-23.718-66.636-64.458-21.54z"></path>
                </g>
            </svg>
            <p>Quer fazer dentes que realmente se pareçam dentes, na prática sem enrolação, você da sua casa e eu daqui de Londrina, PR.</p>
        </div>
    </div>

    <div class="row justify-content-center text-center">
        <h1>Não quer perder tempo!</h1>
    </div>

    <div class="row justify-content-center text-center">
        <div class="col-6">
            <ul>
                <li>Professor Rodrigo Ribeiro é criador do Curso Hands Online.</li>
                <li> Fundou a Update, uma Escola Digital de Atualização em Odontologia específica na formação de Reabilitadores Orais que transformarão a vida de pessoas!</li>
                <li> Ministrou cursos por vários países da América Latina e também como professor de graduação de Odontologia formou mais de 1000 alunos nos últimos 20 anos de sua carreira.</li>
                <li>Formado pela UEL em 2001, Mestre em Dentística, Especialista em Prótese Dentária, Especialista em Periodontia e Técnico em Prótese Dentária, atua hoje como Reabilitador Oral em Londrina, PR e ensina tudo sobre a Reabilitação Oral, com informações e técnicas totalmente validadas com mais de 50 casos clínicos documentados.</li>
                <li>Agora, através do RESET MORFOLÓGICO, vai simplificar o seu caminho para que você tenha resultados expressivos em seus casos clínicos.</li>
            </ul>
        </div>
        <div class="col-6">
            <img class="img-fluid" src="/img/landing-page-2.webp"/>
        </div>
    </div>

    <div class="row justify-content-center text-center my-5">
        <a class="bg-danger text-white py-1 px-3 rounded" href="{{ route('register') }}">{{ __('Quero me inscrever') }}</a>
    </div>
</div>
@endsection
